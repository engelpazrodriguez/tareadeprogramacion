﻿using Nomina.Business.Entities;
using Nomina.Business.Implements;
using Nomina.Views.Extensiones;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nomina.Views
{
    public partial class FrmEmpleado : Form
    {
        public DataSet DSNomina { get; set; }
        private DaoEmpleadoImplements daoEmpleadoImplements;
        private DataRow drEmpleado;

        public FrmEmpleado()
        {
            daoEmpleadoImplements = new DaoEmpleadoImplements();
            InitializeComponent();
        }


        private void button2_Click(object sender, EventArgs e)
        {
            drEmpleado = DSNomina.Tables["Empleado"].NewRow();
            drEmpleado["Cedula"] = "001-100702-2233C";
            drEmpleado["Nombres"] = "Ana Cecilia";
            drEmpleado["Apellidos"] = "Conda Corrales";
            drEmpleado["Direccion"] = "Del ceibon 4 C. al sur";
            drEmpleado["Telefono"] = "8899-7766";
            drEmpleado["FechaContratacion"] = new DateTime(2008, 09, 16);
            drEmpleado["Salario"] = 17400;
           
            Empleado empleado = drEmpleado.Cast<Empleado>();

            if(daoEmpleadoImplements.findByCedula(empleado.Cedula) != null)
            {
                MessageBox.Show("Error, Numero de cedula duplicada", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            daoEmpleadoImplements.Create(empleado);
            DSNomina.Tables["Empleado"].Rows.Add(drEmpleado);
            Dispose();
        }
    }
}
